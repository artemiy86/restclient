package com.example.restclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class RestTemplateService {
    private final RestTemplate restTemplate;
    private final String serverURL;

    @Autowired
    public RestTemplateService(RestTemplate restTemplate, String serverURL) {
        this.restTemplate = restTemplate;
        this.serverURL = serverURL;
    }


}
