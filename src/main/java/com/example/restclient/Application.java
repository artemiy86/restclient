package com.example.restclient;

import com.example.restclient.Entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {

        String str = new String();

        SpringApplication.run(Application.class, args);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> entity = restTemplate.getForEntity("http://91.241.64.178:7081/api/users",String.class);
        List<String> cookies = entity.getHeaders().get("Set-Cookie");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie",cookies.stream().collect(Collectors.joining((";"))));
        User user = new User(3L,"James","Brown",(byte)27);
        HttpEntity<User> newEntity = new HttpEntity<User>(user, headers);
        ResponseEntity<String> result = restTemplate.exchange("http://91.241.64.178:7081/api/users", HttpMethod.POST,newEntity, String.class);
        System.out.println(result.getBody());
        str +=  result.getBody();
        user.setName("Thomas");
        user.setLastName("Shelby");
        HttpEntity<User> newEntity2 = new HttpEntity<User>(user, headers);
        ResponseEntity<String> result2 = restTemplate.exchange("http://91.241.64.178:7081/api/users", HttpMethod.PUT,newEntity2, String.class);
        System.out.println(result2.getBody());
        str +=  result2.getBody();
        HttpEntity<User> newEntity3 = new HttpEntity<User>(headers);
        ResponseEntity<String> result3 = restTemplate.exchange("http://91.241.64.178:7081/api/users/3", HttpMethod.DELETE,newEntity3, String.class);
        System.out.println(result3.getBody());
        str +=  result3.getBody();
        System.out.println(str);
    }

}
